export enum Status {
  INITIALIZATION = 0,
  RUNNING = 1,
  COMPLETED = 2
}

export class Step {
  id: number;
  task_id: number;
  schedule_id: number;
  run_id: number;
  step_sequence: number;
  step_script: string;
  status: Status = Status.INITIALIZATION;
  created_at: any;
  completed_at: any;

// tslint:disable-next-line: max-line-length
  constructor(id?: number, task_id?: number, schedule_id?: number, run_id?: number, step_sequence?: number, step_script?: string, status?: Status, created_at?: any, completed_at?: any) {
    this.id = id;
    this.task_id = task_id;
    this.schedule_id = schedule_id;
    this.run_id = run_id;
    this.step_sequence = step_sequence;
    this.step_script = step_script;
    this.status = status;
    this.created_at = created_at;
    this.completed_at = completed_at;
  }
}

export class StepService {

  private TABLE = 'step';

  constructor(private knex) {
  }

  create(): Promise<any> {
    return this.knex.schema.hasTable(this.TABLE).then(exists => {
      if (!exists) {
        return this.knex.schema.createTable(this.TABLE, (table) => {
          table.increments('id').primary();
          table.integer('task_id').notNullable();
          table.integer('schedule_id').notNullable();
          table.integer('run_id').notNullable();
          table.string('step_sequence').notNullable();
          table.string('step_script').notNullable();
          table.integer('status').defaultTo(Status.INITIALIZATION).notNullable();
          table.timestamp('created_at').defaultTo(this.knex.fn.now()).notNullable();
          table.timestamp('completed_at');
        });
      }
    });
  }

  selectByRunId(run_id: number): Promise<any> {
    return this.knex.select().from(this.TABLE).where('run_id', '=', run_id).orderBy('step_sequence');
  }

  insert(args: any): Promise<any> {
    return this.knex(this.TABLE).insert(args);
  }

  update(args: any): Promise<any> {
    return this.knex(this.TABLE).where('id', '=', args.id).update(args);
  }

  deleteByRunId(run_id: number): Promise<any> {
    return this.knex(this.TABLE).where('run_id', '=', run_id).del();
  }

  next(run_id: number, next_step_sequence: number): Promise<any>  {
    return this.knex.select().from(this.TABLE).where({
      'run_id': run_id,
      'step_sequence': next_step_sequence
    });
  }

  updateStatus(id: number, status: Status) {
    return this.knex(this.TABLE).where('id', '=', id).update({
      status: status
    });
  }

}

