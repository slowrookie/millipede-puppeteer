import { app, ipcMain } from 'electron';
import * as fs from 'fs';
import { EVENTS } from '../../shared/events';
import { TaskService } from './task-service';
import { ScheduleService } from './schedule-service';
import { Page } from '../../shared/page';
import * as settings from 'electron-settings';
import { CONSTANTS } from '../../shared/constants';
import { RunService } from './run-service';
import { StepService } from './step-service';
import { DataService } from './data-service';

declare var Promise: any;


let db_path;
export let knex;
export let taskService: TaskService;
export let scheduleService: ScheduleService;
export let runService: RunService;
export let stepService: StepService;
export let dataService: DataService;

// tslint:disable-next-line: no-shadowed-variable
function createKnex(db_path: string) {
  return require('knex')({
    client: 'sqlite3',
    connection: {
      filename: db_path + '/database.sqlite'
    },
    useNullAsDefault: true,
    log: {
      warn(message) {
        console.log(message);
      },
      error(message) {
        console.log(message);
      },
      deprecate(message) {
        console.log(message);
      },
      debug(message) {
        console.log(message);
      },
    },
    debug: false
  });
}

// 数据库初始化
ipcMain.on(EVENTS.DB_INITIALIZE, (event, args) => {

  db_path = app.getPath('userData') + '/millipede';

  if (!fs.existsSync(db_path)) {
    fs.mkdirSync(db_path);
  }

  settings.set(CONSTANTS.DB_PATH, app.getPath('userData') + '/millipede');

  knex = createKnex(db_path);

  taskService = taskService || new TaskService(knex);
  scheduleService = scheduleService || new ScheduleService(knex);
  runService = runService || new RunService(knex);
  stepService = stepService || new StepService(knex);
  dataService = dataService || new DataService(knex);

  event.sender.send(EVENTS.DB_INITIALIZE_SUCCESS, !!knex);
});

ipcMain.on(EVENTS.DB_CHECK_TABLES, (event, args) => {
  Promise.all([
    taskService.create(),
    scheduleService.create(),
    runService.create(),
    stepService.create()
  ])
    .then(() => event.sender.send(EVENTS.DB_CHECK_TABLES_SUCCESS, true))
    .catch(e => event.sender.send(EVENTS.DB_ERROR, `${EVENTS.DB_CHECK_TABLES} ${e.toString()}`));
});


// 任务
ipcMain.on(EVENTS.DB_SELECT_TASK, (event, page: Page) => {
  taskService.select(page)
    .then(values => {
      values[0].map(row => row.steps = row.steps ? JSON.parse(row.steps) : row.steps);
      event.sender.send(EVENTS.DB_SELECT_TASK_SUCCESS, { data: values[0], count: values[1][0]['count'] || 0 });
    })
    .catch(e => event.sender.send(EVENTS.DB_ERROR, `${EVENTS.DB_SELECT_TASK} ${e.toString()}`));
});

ipcMain.on(EVENTS.DB_SELECT_TASK_BY_ID, (event, args) => {
  taskService.selectById(args)
    .then(rows => {
      rows.map(row => row.steps = row.steps ? JSON.parse(row.steps) : row.steps);
      event.sender.send(EVENTS.DB_SELECT_TASK_BY_ID_SUCCESS, rows);
    })
    .catch(e => event.sender.send(EVENTS.DB_ERROR, `${EVENTS.DB_SELECT_TASK_BY_ID} ${e.toString()}`));
});

ipcMain.on(EVENTS.DB_EXISTS_TASK_BY_NAME, (event, args) => {
  taskService.selectExistsName(args.name, args.id)
    .then(row => event.sender.send(EVENTS.DB_EXISTS_TASK_BY_NAME_SUCCESS, row))
    .catch(e => event.sender.send(EVENTS.DB_ERROR, `${EVENTS.DB_EXISTS_TASK_BY_NAME} ${e.toString()}`));
});

ipcMain.on(EVENTS.DB_INSERT_TASK, (event, args) => {
  taskService.insert(args)
    .then(() => event.sender.send(EVENTS.DB_INSERT_TASK_SUCCESS))
    .catch(e => event.sender.send(EVENTS.DB_ERROR, `${EVENTS.DB_INSERT_TASK} ${e.toString()}`));
});

ipcMain.on(EVENTS.DB_UPDATE_TASK, (event, args) => {
  taskService.update(args)
    .then(() => event.sender.send(EVENTS.DB_UPDATE_TASK_SUCCESS))
    .catch(e => event.sender.send(EVENTS.DB_ERROR, `${EVENTS.DB_UPDATE_TASK} ${e.toString()}`));
});

ipcMain.on(EVENTS.DB_DELETE_TASK, (event, args) => {
  scheduleService.selectByTaskId(args)
    .then((schedules: Array<any>) => Promise.all(
      schedules.map(schedule => deleteDataStepRunAndSchedule(schedule.id))
    ))
    .then(() => taskService.delete(args))
    .then(() => event.sender.send(EVENTS.DB_DELETE_TASK_SUCCESS))
    .catch(e => event.sender.send(EVENTS.DB_ERROR, `${EVENTS.DB_DELETE_TASK} ${e.toString()}`));
});

// 任务执行计划
ipcMain.on(EVENTS.DB_SELECT_SCHEDULE, (event, args) => {
  scheduleService.selectByTaskId(args.task_id)
    .then(rows => event.sender.send(EVENTS.DB_SELECT_SCHEDULE_SUCCESS, rows))
    .catch(e => event.sender.send(EVENTS.DB_ERROR, `${EVENTS.DB_SELECT_SCHEDULE} ${e.toString()}`));
});

ipcMain.on(EVENTS.DB_INSERT_SCHEDULE, (event, args) => {
  scheduleService.insert(args)
    .then(() => event.sender.send(EVENTS.DB_INSERT_SCHEDULE_SUCCESS))
    .catch(e => event.sender.send(EVENTS.DB_ERROR, `${EVENTS.DB_INSERT_SCHEDULE} ${e.toString()}`));
});

ipcMain.on(EVENTS.DB_UPDATE_SCHEDULE, (event, args) => {
  scheduleService.update(args)
    .then(() => event.sender.send(EVENTS.DB_UPDATE_SCHEDULE_SUCCESS))
    .catch(e => event.sender.send(EVENTS.DB_ERROR, `${EVENTS.DB_UPDATE_SCHEDULE} ${e.toString()}`));
});

ipcMain.on(EVENTS.DB_DELETE_SCHEDULE, (event, args) => {
  deleteDataStepRunAndSchedule(args)
    .then(() => event.sender.send(EVENTS.DB_DELETE_SCHEDULE_SUCCESS))
    .catch(e => event.sender.send(EVENTS.DB_ERROR, `${EVENTS.DB_DELETE_SCHEDULE} ${e.toString()}`));
});

// 任务执行计划记录
ipcMain.on(EVENTS.DB_SELECT_RUN, (event, args: Page) => {
  runService.select(args)
    .then(rows => event.sender.send(EVENTS.DB_SELECT_RUN_SUCCESS + args.params.scheduleId, rows))
    .catch(e => event.sender.send(EVENTS.DB_ERROR, `${EVENTS.DB_SELECT_RUN} ${e.toString()}`));
});

ipcMain.on(EVENTS.DB_DELETE_RUN, (event, args) => {
  deleteDataStepAndRun(args.runId)
    .then(() => event.sender.send(EVENTS.DB_DELETE_RUN_SUCCESS + args.scheduleId))
    .catch(e => event.sender.send(EVENTS.DB_ERROR, `${EVENTS.DB_DELETE_RUN} ${e.toString()}`));
});

// 任务执行计划步骤
ipcMain.on(EVENTS.DB_SELECT_STEP, (event, args) => {
  stepService.selectByRunId(args)
    .then(rows => event.sender.send(EVENTS.DB_SELECT_STEP_SUCCESS, rows))
    .catch(e => event.sender.send(EVENTS.DB_ERROR, `${EVENTS.DB_SELECT_STEP} ${e.toString()}`));
});

// 查询数据
ipcMain.on(EVENTS.DB_SELECT_DATA, (event, args) => {
  dataService.select(args.tableName, args.page)
    .then(rows => event.sender.send(EVENTS.DB_SELECT_DATA_SUCCESS, rows))
    // .catch(e => event.sender.send(EVENTS.DB_ERROR, `${EVENTS.DB_SELECT_DATA} ${e.toString()}`))
    .catch(e => event.sender.send(EVENTS.DB_SELECT_DATA_SUCCESS, [[], [{count: 0}]]));
});

function deleteDataStepRunAndSchedule(schduleId: number): Promise<any> {
  return runService.selectByScheduleId(schduleId)
    // delete runs
    .then((runs: Array<any>) => Promise.all(
      runs.map(run => deleteDataStepAndRun(run.id))
    ))
    // delete schedule
    .then(() => scheduleService.delete(schduleId));
}

function deleteDataStepAndRun(runId: number): Promise<any> {
  return stepService.selectByRunId(runId)
    // delete data table
    .then((steps: Array<any>) => Promise.all(
      steps.map(step => dataService.delete(dataService.tableName(step)))
    ))
    // delete steps
    .then(() => stepService.deleteByRunId(runId))
    // delete run
    .then(() => runService.delete(runId));
}
