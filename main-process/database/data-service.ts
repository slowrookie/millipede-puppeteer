import { Page } from '../../shared/page';
import { Step } from './step-service';

export class DataService {

  constructor(private knex) { }

  tableName(step: Step) {
    return `data_${step.task_id}_${step.schedule_id}_${step.run_id}_${step.id}`;
  }

  create(tn: string): Promise<any> {
    return this.knex.schema.hasTable(tn).then(exists => {
      if (!exists) {
        return this.knex.schema.createTable(tn, table => {
          table.increments('id').primary();
          table.integer('source_id');
          table.string('result');
          table.boolean('status');
          table.string('message');
          table.timestamp('created_at').defaultTo(this.knex.fn.now()).notNullable();
          table.timestamp('completed_at');
        });
      }
    });
  }

  select(tn: string, page?: Page): Promise<any> {
    const query = this.knex.select().from(tn);

    function condition(promise: any) {
      page.sorts.forEach(sort => promise.orderBy(sort.active, sort.direction));
      return promise;
    }

    if (page) {
      const count = this.knex(tn).count('id as count');
      return Promise.all([
        condition(query).limit(page.limit).offset(page.offset),
        condition(count)
      ]);
    } else {
      return query;
    }

  }

  insert(tn: string, data: any): Promise<any> {
    return this.knex(tn).insert(data);
  }

  delete(tn: string): Promise<any> {
    return this.knex.schema.hasTable(tn).then(b => b ? this.knex.schema.dropTable(tn) : false );
  }

}

export class Data {
  id: number;
  source_id: number;
  result: string;
  status: boolean;
  created_at: any;
  completed_at: any;

  constructor(id?: number, source_id?: number, result?: string, status?: boolean, created_at?: any, completed_at?: any) {
    this.id = id;
    this.source_id = source_id;
    this.result = result;
    this.status = status;
    this.created_at = created_at;
    this.completed_at = completed_at;
  }
}
