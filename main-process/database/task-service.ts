import { Page } from '../../shared/page';

export class TaskService {

  private TABLE = 'task';

  constructor(private knex) {}

  create(): Promise<any> {
    return this.knex.schema.hasTable(this.TABLE).then(exists => {
      if (!exists) {
        return this.knex.schema.createTable(this.TABLE, (table) => {
          table.increments('id').primary();
          table.string('uuid').unique();
          table.string('name').unique();
          table.string('desc');
          table.json('steps');
          table.timestamp('created_at').defaultTo(this.knex.fn.now()).notNullable();
          table.timestamp('modify_at').defaultTo(this.knex.fn.now()).notNullable();
        });
      }
    });
  }

  select(page: Page): Promise<any> {
    const count = this.knex(this.TABLE).count('id as count');
    const query = this.knex.select().from(this.TABLE);

    function condition(promise: any) {
      page.sorts.forEach(sort => promise.orderBy(sort.active, sort.direction));
      return promise;
    }

    return Promise.all([
      condition(query).limit(page.limit).offset(page.offset),
      condition(count)
    ]);
  }

  selectById(id: number): Promise<any> {
    return this.knex.select().from(this.TABLE).where('id', '=', id);
  }

  selectExistsName(name: string, id?: number): Promise<any> {
    const query = this.knex.select().from(this.TABLE).where('name', '=', name);
    if (id) {
      query.andWhere('id', '!=', id);
    }
    return query;
  }

  insert(task: any): Promise<any> {
    task.steps = JSON.stringify(task.steps);
    return this.knex(this.TABLE).insert(task);
  }

  update(task: any): Promise<any> {
    task.steps = JSON.stringify(task.steps);
    return this.knex(this.TABLE).where('id', '=', task.id).update(task);
  }

  delete(id: number): Promise<any> {
    return this.knex(this.TABLE).where('id', '=', id).del();
  }

}

export class Task {
  id: number;
  uuid: string;
  name: string;
  desc: string;
  steps: Array<Step> = [];
  modify_at: any;
  created_at: any;

  constructor(id?: number, uuid?: string, name?: string, desc?: string, steps?: Array<Step>, modify_at?: any, created_at?: any) {
    this.id = id;
    this.uuid = uuid;
    this.name = name;
    this.desc = desc;
    this.steps = steps;
    this.modify_at = modify_at;
    this.created_at = created_at;
  }
}

export class Step {
  script: string;
  sequence: number;

  constructor(script: string, sequence: number) {
    this.script = script;
    this.sequence = sequence;
  }

}
