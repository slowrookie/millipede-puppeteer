import { Page } from '../../shared/page';


export enum Status {
  INITIALIZATION = 0,
  RUNNING = 1,
  COMPLETED = 2
}

export class Run {
  id: number;
  task_id: number;
  schedule_id: number;
  status: Status = Status.INITIALIZATION;
  created_at: any;
  completed_at: any;

  constructor(id?: number, task_id?: number, schedule_id?: number, status?: Status, created_at?: any, completed_at?: any) {
    this.id = id;
    this.task_id = task_id;
    this.schedule_id = schedule_id;
    this.status = status;
    this.created_at = created_at;
    this.completed_at = completed_at;
  }
}


export class RunService {

  private TABLE = 'run';

  constructor(private knex) {
  }

  create(): Promise<any> {
    return this.knex.schema.hasTable(this.TABLE).then(exists => {
      if (!exists) {
        return this.knex.schema.createTable(this.TABLE, (table) => {
          table.increments('id').primary();
          table.integer('task_id').notNullable();
          table.integer('schedule_id').notNullable();
          table.integer('status').defaultTo(Status.INITIALIZATION).notNullable();
          table.timestamp('created_at').defaultTo(this.knex.fn.now()).notNullable();
          table.timestamp('completed_at');
        });
      }
    });
  }

  select(page: Page): Promise<any> {
    const count = this.knex(this.TABLE).count('id as count');
    const query = this.knex.select().from(this.TABLE);

    function condition(promise: any) {
      if (page.params.scheduleId) { promise.where('schedule_id', '=', page.params.scheduleId); }
      page.sorts.forEach(sort => promise.orderBy(sort.active, sort.direction));
      return promise;
    }

    return Promise.all([
      condition(query).limit(page.limit).offset(page.offset),
      condition(count)
    ]);
  }

  selectByScheduleId(scheduleId: number) {
    return this.knex.select().from(this.TABLE).where('schedule_id', '=', scheduleId);
  }

  insert(args: any): Promise<any> {
    return this.knex(this.TABLE).insert(args);
  }

  update(args: any): Promise<any> {
    return this.knex(this.TABLE).where('id', '=', args.id).update(args);
  }

  delete(id: any): Promise<any> {
    return this.knex(this.TABLE).where('id', '=', id).del();
  }

  updateStatus(id: number, status: Status) {
    return this.knex(this.TABLE).where('id', '=', id).update({
      status: status
    });
  }

}
