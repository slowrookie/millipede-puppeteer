export enum Category {
  MANUAL = 'manual',
  DATETIME = 'datetime',
  PERIOD = 'period',
  CRON = 'cron'
}

export class Schedule {
  id: number;
  task_id: number;
  name: string;
  category: Category = Category.MANUAL;
  datetime: any;
  period: any;
  time: any;
  cron: string;
  enabled: boolean;
  created_at: any = new Date();

// tslint:disable-next-line: max-line-length
  constructor(id?: number, task_id?: number, name?: string, category?: Category, time?: any, period?: any, datetime?: any, cron?: string, enabled?: boolean, created_at?: any) {
    this.id = id;
    this.task_id = task_id;
    this.name = name;
    this.category = category;
    this.period = period;
    this.time = time;
    this.datetime = datetime;
    this.cron = cron;
    this.enabled = enabled;
    this.created_at = created_at;
  }
}

export class ScheduleService {

  private TABLE = 'schedule';

  constructor(private knex) {
  }

  create(): Promise<any> {
    return this.knex.schema.hasTable(this.TABLE).then(exists => {
      if (!exists) {
        return this.knex.schema.createTable(this.TABLE, (table) => {
          table.increments('id').primary();
          table.integer('task_id');
          table.string('name').notNullable();
          table.string('category').notNullable();
          table.timestamp('datetime');
          table.string('period');
          table.timestamp('time');
          table.string('cron');
          table.boolean('enabled').defaultTo(false).notNullable();
          table.timestamp('created_at').defaultTo(this.knex.fn.now()).notNullable();
        });
      }
    });
  }

  selectById(id: number): Promise<any> {
    return this.knex.select().from(this.TABLE).where('id', '=', id);
  }

  selectByTaskId(task_id: number): Promise<any> {
    return this.knex.select().from(this.TABLE).where('task_id', '=', task_id);
  }

  insert(args: any): Promise<any> {
    return this.knex(this.TABLE).insert(args);
  }

  update(args: any): Promise<any> {
    return this.knex(this.TABLE).where('id', '=', args.id).update(args);
  }

  delete(args: any): Promise<any> {
    return this.knex(this.TABLE).where('id', '=', args).del();
  }

}

