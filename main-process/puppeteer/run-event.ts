export enum  RunEvents {
  INSERT_DATA_TABLE = 'INSERT_DATA_TABLE',
  STEP_COMPLETED = 'STEP_COMPLETED',
  CHILDPROCESS_SAVE_DATA = 'CHILDPROCESS_SAVE_DATA'
}

export class RunMessage {
  event: RunEvents;
  msg: any;

  constructor(event: RunEvents, msg: any) {
    this.event = event;
    this.msg = msg;
  }

}
