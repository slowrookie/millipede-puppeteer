import { RunEvents, RunMessage } from './run-event';
import { Data } from '../database/data-service';
import { Step } from '../database/step-service';
import { from, Observable, combineLatest, Observer } from 'rxjs';
import * as fs from 'fs';
import * as os from 'os';

const step: Step = JSON.parse(process.argv[3]);
const preStepDataPath: string = process.argv[4];
let preStepDataArray: Array<Data> = [];
const start = new Date();

process.env.PUPPETEER_EXECUTABLE_PATH = process.argv[2];

if (preStepDataPath && preStepDataPath !== 'undefined') {
  preStepDataArray = JSON.parse(fs.readFileSync(preStepDataPath).toString());
}

function exec() {
  let obs = null;
  if (step.step_sequence === 0) {
    obs = script();
  } else {
    const count = preStepDataArray.length >= os.cpus().length ?
      os.cpus().length : preStepDataArray.length <= 0 ? 1 : preStepDataArray.length;
    obs = combineLatest(Array(count).fill(1).map(() => script(preStepDataArray.pop())));
  }

  obs.subscribe(
    next => {
      preStepDataArray.length ? exec() : process.exit();
    }
  );
}

// 异步并行执行
function script($$Data?: Data): Observable<any> {
  const sourceId = $$Data ? $$Data.id : null;
  return new Observable((obs: Observer<any>) => {
// tslint:disable-next-line: no-shadowed-variable
    const script = `
    const $$Params = ${$$Data ? $$Data.result : null};
    const $$SourceId = ${sourceId};
    const $$Start = new Date();

    function $$Save(result, status = true) {
      let data = {
        source_id: $$SourceId,
        result: JSON.stringify(result),
        status: status,
        created_at: $$Start,
        completed_at: new Date()
      }
      process.send({event: 'INSERT_DATA_TABLE', msg: data});
    }

    ${step.step_script}
    `;
    try {
// tslint:disable-next-line: no-eval
      from(eval(script)).subscribe(
        next => { obs.next(next); },
        e => {
          $$Save(e.toString(), false, sourceId);
          obs.next(e);
        }
      );
    } catch (e) {
      $$Save(e.toString(), false, sourceId);
      obs.next(e);
    }
  });
}

function $$Save(result: any, status: boolean = true, sourceId?: number) {
  const data = new Data(null, sourceId, JSON.stringify(result), status, start, new Date());
  process.send(new RunMessage(RunEvents.INSERT_DATA_TABLE, data));
}

exec();
