import { ipcMain, Event, app } from 'electron';
import { from, Subscription, Subject, Observable, combineLatest } from 'rxjs';
import { takeUntil, flatMap, map } from 'rxjs/operators';
import { EVENTS } from '../../shared/events';
import * as childProcess from 'child_process';
import * as fs from 'fs';
import * as path from 'path';
import { taskService, scheduleService, runService, stepService, dataService } from '../database/db-service';
import { Schedule } from '../database/schedule-service';
import { RunMessage, RunEvents } from './run-event';
import { Run } from '../database/run-service';
import { Status, Step } from '../database/step-service';
import { Task } from '../database/task-service';
import { CONSTANTS } from '../../shared/constants';

process.setMaxListeners(0);

function getPuppeteerExecutablePath(): string {
  const settings = require('electron-settings');
  const puppeteerExecutablePath = settings.get(CONSTANTS.PUPPETEER_EXECUTABLE_PATH);
  process.env.PUPPETEER_EXECUTABLE_PATH = puppeteerExecutablePath;
  return puppeteerExecutablePath;
}

// 执行测试脚本
const stopObject: Subject<any> = new Subject<any>();
const stopObasrvable: Observable<any> = stopObject.asObservable();
let subscription: Subscription = null;

ipcMain.on(EVENTS.PUPPETEER_TEST_RUN, (event: Event, args) => {
  getPuppeteerExecutablePath();
  try {
    // tslint:disable-next-line: no-eval
    subscription = from(eval(args))
      .pipe(
        takeUntil(stopObasrvable)
      )
      .subscribe(
        n => event.sender.send(EVENTS.PUPPETEER_TEST_RUN_SUCCESS, { success: true, data: n }),
        e => event.sender.send(EVENTS.PUPPETEER_TEST_RUN_SUCCESS, { success: false, data: e.toString() }),
      );
  } catch (e) {
    event.sender.send(EVENTS.PUPPETEER_TEST_RUN_SUCCESS, { success: false, data: e.toString() });
  }

});

ipcMain.on(EVENTS.PUPPETEER_TEST_STOP, (event, args) => {
  stopObject.next('stop');
  if (subscription) { subscription.unsubscribe(); }
  event.sender.send(EVENTS.PUPPETEER_TEST_STOP_SCUCCESS, true);
});


// 手动运行
ipcMain.on(EVENTS.PUPPETEER_MANUAL_RUN, (event, args) => {
  prepared(args).subscribe(steps => {
    const step: Step = steps[0];
    event.sender.send(EVENTS.PUPPETEER_MANUAL_RUN_PREPARED);
    childProcessRun(event, step);
  });
});

function prepared(scheduleId: number): Observable<any> {
  let schedule: Schedule;
  let task: Task;
  let runId: number;
  return from(scheduleService.selectById(scheduleId)).pipe(  // schedule
    map(rows => {
      schedule = rows[0];
      return schedule;
    }),
    // tslint:disable-next-line: no-shadowed-variable
    flatMap((schedule: Schedule) => { // schedule
      return from(taskService.selectById(schedule.task_id));
    }),
    flatMap(rows => { // create task_schedule_run
      task = rows[0];
      task.steps = JSON.parse(task.steps.toString());
      const scheduleRun: Run = new Run(null, task.id, schedule.id, Status.INITIALIZATION, new Date(), null);
      return from(runService.insert(scheduleRun));
    }),
    flatMap(r => { // create task_schedule_run_step
      runId = r[0];
      return combineLatest(
        task.steps.map(step => {
          // tslint:disable-next-line: max-line-length
          const scheduleRunStep = new Step(null, task.id, schedule.id, runId, step.sequence, step.script, Status.INITIALIZATION, new Date(), null);
          return from(stepService.insert(scheduleRunStep));
        })
      );
    }),
    flatMap(() => { // select task_schedule_run_step
      return from(stepService.selectByRunId(runId));
    })
  );
}

function childProcessRun(event, step: Step, args?: string) {
  const tableName = dataService.tableName(step);
  const puppeteerExecutablePath = getPuppeteerExecutablePath();

  from(dataService.create(tableName)).pipe(
    flatMap(() => runService.updateStatus(step.run_id, Status.RUNNING)),
    flatMap(() => stepService.updateStatus(step.id, Status.RUNNING)),
    map(() => event.sender.send(EVENTS.PUPPETEER_MANUAL_RUN_STATUS + step.schedule_id))
  ).subscribe(() => {
    const processServicePath = path.join(__dirname, 'process-service.js');
    const worker_process = childProcess.fork( processServicePath, [puppeteerExecutablePath, JSON.stringify(step), args]);
    worker_process.on('close', code => {
      console.log(`子进程关闭 ${process.pid}`);
      deleteFile(tableName);
    });
    worker_process.on('exit', code => {
      console.log(`子进程退出: ${process.pid}`);
      next(event, tableName, step);
    });
    worker_process.on('message', (message: RunMessage) => {
      if (message.event === RunEvents.INSERT_DATA_TABLE) {
        from(dataService.insert(tableName, message.msg)).subscribe(msg => console.log('insert'));
      }
    });
  });
}

function next(event, tableName: string, step: Step) {

  // update status
  from(stepService.updateStatus(step.id, Status.COMPLETED)).pipe(
    flatMap(() => from(stepService.next(step.run_id, step.step_sequence * 1 + 1)))
  ).subscribe(rows => {
    if (rows.length) {
      from(dataService.select(tableName)).subscribe(data => {
        childProcessRun(event, rows[0], writeFile(tableName, data));
      });
    } else {
      from(runService.updateStatus(step.run_id, Status.COMPLETED)).subscribe(() => {
        event.sender.send(EVENTS.PUPPETEER_MANUAL_RUN_STATUS + step.schedule_id);
      });
    }
  });
}

function writeFile(tableName: string, data: any) {
  const dir = path.join(app.getPath('userData'), 'tmp');
  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
  }

  const preStepDataPath = path.join(dir, `${tableName}.tmp`);
  fs.writeFileSync(preStepDataPath, JSON.stringify(data));
  return preStepDataPath;
}

function deleteFile(tableName: string) {
  const dir = path.join(app.getPath('userData'), 'tmp');
  const preStepDataPath = path.join(dir, `${tableName}.tmp`);
  // tslint:disable-next-line: deprecation
  fs.exists(preStepDataPath, bool => {
    if (bool) { fs.unlinkSync(preStepDataPath); }
  });
}
