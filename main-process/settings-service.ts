import { ipcMain } from 'electron';
import { EVENTS } from '../shared/events';
import * as settings from 'electron-settings';

ipcMain.on(EVENTS.SETTINGS_SAVE, (event, args: Array<any>) => {
  for (let index = 0; index < args.length; index++) {
    const arg = args[index];
// tslint:disable-next-line: forin
    for (const key in arg) {
      settings.set(key, arg[key]);
    }
  }

  event.sender.send(EVENTS.SETTINGS_SAVE_SUCCESS, args);
});

ipcMain.on(EVENTS.SETTINGS_GET, (event, args: string) => {
  event.sender.send(EVENTS.SETTINGS_GET_SUCCESS, settings.get(args));
});
