import { Component, OnInit, OnDestroy } from '@angular/core';
import { ElectronService } from '../../providers/electron.service';
import { CONSTANTS } from '../../../../shared/constants';
import { EVENTS } from '../../../../shared/events';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit, OnDestroy {

  data = ['settings'];
  versions = [];
  chrome = '';
  isLocked = true;
  isLoading = false;

  constructor(private electronService: ElectronService) {
    this.electronService.ipcRenderer.send(EVENTS.SETTINGS_GET, CONSTANTS.PUPPETEER_EXECUTABLE_PATH);

    this.settings();
    this.electronService.ipcRenderer.on(EVENTS.SETTINGS_GET_SUCCESS, (event, args) => {
      this.chrome = args;
    });

    this.electronService.ipcRenderer.on(EVENTS.SETTINGS_SAVE_SUCCESS, (event, args) => {
      this.isLoading = false;
    });

  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.electronService.removeAllListeners(EVENTS.SETTINGS_GET_SUCCESS);
  }

  settings() {
    this.versions.push({ key: 'millipede', value: '0.0.1' });
    this.versions.push({ key: 'puppeteer', value: '1.10.0' });

    for (const key in process.versions) {
      if (['electron', 'v8', 'node'].includes(key)) {
        this.versions.push({ key: key, value: process.versions[key] });
      }
    }

    this.versions.push({ key: 'architecture', value: process.arch });

    // capitalize the first letter
    this.versions.forEach(v => {
      v.key = v.key.charAt(0).toUpperCase() + v.key.slice(1);
    });
  }

  onSetChromium() {
    this.isLocked = !this.isLocked;
    if (this.isLocked) {
      this.isLoading = true;
      const o = {};
      this.chrome = this.chrome.trim();
      o[CONSTANTS.PUPPETEER_EXECUTABLE_PATH] = this.chrome;
      this.electronService.ipcRenderer.send(EVENTS.SETTINGS_SAVE, [o]);
    }
  }

  onShell(href) {
    this.electronService.shell.openExternal(href);
  }

}
