import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { FormGroup, FormBuilder, Validators, FormControl, ValidationErrors } from '@angular/forms';
import { Observable, Observer, merge } from 'rxjs';
import { delay } from 'rxjs/operators';
import { v4 as UUID } from 'uuid';
import { Task, Step } from '../../../../../main-process/database/task-service';
import { ElectronService } from '../../../providers/electron.service';
import { EVENTS } from '../../../../../shared/events';
import { LoaderService, Loader } from '../../../providers/loader.service';
import { NzModalService } from 'ng-zorro-antd';

@Component({
  selector: 'app-task-edit',
  templateUrl: './task-edit.component.html',
  styleUrls: ['./task-edit.component.scss']
})
export class TaskEditComponent implements OnInit, OnDestroy {

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private formBuilder: FormBuilder,
    private electron: ElectronService,
    private loader: LoaderService,
    private modal: NzModalService
  ) {
    this.validateForm = formBuilder.group({
      name: ['', [Validators.required], [this.nameAsyncValidator]],
      desc: ['', [Validators.required]]
    });
  }

  task: Task = new Task();
  validateForm: FormGroup;
  tabSelectedindex: number;
  taskModified = false;


  ngOnInit() {
    this.route.paramMap.subscribe(paramMap => {
      if (paramMap.keys.length) {
        this.electron.ipcRenderer.send(EVENTS.DB_SELECT_TASK_BY_ID, paramMap.get('id'));
      }
    });

    this.electron.on(EVENTS.DB_SELECT_TASK_BY_ID_SUCCESS).subscribe(args => {
      args[0].steps = args[0].steps || [];
      this.task = args[0];
      this.validateForm.patchValue(this.task);
    });

    merge(
      this.electron.on(EVENTS.DB_INSERT_TASK_SUCCESS),
      this.electron.on(EVENTS.DB_UPDATE_TASK_SUCCESS)
    ).pipe(delay(200)).subscribe(() => this.loader.load(new Loader(false)));

  }

  ngOnDestroy() {
    this.electron.removeAllListeners(
      EVENTS.DB_SELECT_TASK_BY_ID_SUCCESS,
      EVENTS.DB_EXISTS_TASK_BY_NAME_SUCCESS,
      EVENTS.DB_INSERT_TASK_SUCCESS,
      EVENTS.DB_UPDATE_TASK_SUCCESS
    );
  }

  onTaskSave($event: MouseEvent) {
    this.loader.load(new Loader(true, '保存中...'));
    if (this.task.id) {
      this.task.modify_at = new Date();
    } else {
      this.task.uuid = UUID();
      this.task.created_at = new Date();
      this.task.modify_at = new Date();
    }
    this.electron.ipcRenderer.send(this.task.id ? EVENTS.DB_UPDATE_TASK : EVENTS.DB_INSERT_TASK, this.task);
  }

  onAddStep($event: MouseEvent) {
    const steps = this.task.steps || [];
    const sequence: number = steps.length <= 0 ? 0 : steps[steps.length - 1].sequence + 1;
    steps.push({ sequence: sequence, script: '' });
    this.task.steps = steps;
    this.tabSelectedindex = steps.length - 1;
  }

  onRemoveStep($event: MouseEvent, index: number) {
    this.modal.confirm({
      nzTitle: `确认删除 "步骤 ${index + 1}"？`,
      nzOnOk: () => {
        const steps = this.task.steps;
// tslint:disable-next-line: no-shadowed-variable
        steps.splice(index, 1).map((step: Step, index: number) => {
          step.sequence = index;
        });
        this.task.steps = steps;
      }
    });
  }

  nameAsyncValidator = (control: FormControl) => new Observable((observer: Observer<ValidationErrors>) => {
    this.electron.ipcRenderer.send(EVENTS.DB_EXISTS_TASK_BY_NAME, { name: control.value, id: this.task.id });
    this.electron.once(EVENTS.DB_EXISTS_TASK_BY_NAME_SUCCESS).subscribe(args => {
      observer.next(args.length ? { error: true, duplicated: true } : null);
      observer.complete();
    });
  })

  onBack($event: MouseEvent) {
    this.location.back();
  }


}
