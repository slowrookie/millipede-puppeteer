import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { LoaderService, Loader } from '../../providers/loader.service';
import { ElectronService } from '../../providers/electron.service';
import { EVENTS } from '../../../../shared/events';
import { merge } from 'rxjs';
import { Task } from '../../../../main-process/database/task-service';
import { Page } from '../../../../shared/page';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss']
})
export class TaskComponent implements OnInit, OnDestroy {

  constructor(
    private router: Router,
    private loader: LoaderService,
    private electron: ElectronService
  ) {}

  pageIndex = 0;
  PAGE_SIZE = 50;
  tasks: Array<Task> = [];
  page: Page = new Page(this.PAGE_SIZE, 0, {}, [{active: 'created_at', direction: 'desc'}]);


  ngOnInit(): void {
    this.loader.load(new Loader(true, '加载任务列表...'));
    this.electron.ipcRenderer.send(EVENTS.DB_SELECT_TASK, this.page);

    this.electron.on(EVENTS.DB_SELECT_TASK_SUCCESS).subscribe(d => {
      this.tasks = this.tasks.concat(d.data);
      this.page.total = d.count;
      this.loader.load(new Loader(false));
    });

    merge(
      this.electron.on(EVENTS.DB_DELETE_TASK_SUCCESS)
    ).subscribe(() => {
      this.tasks = [];
      this.page.offset = 0;
      this.electron.ipcRenderer.send(EVENTS.DB_SELECT_TASK, this.page);
    });
  }

  ngOnDestroy(): void {
    this.electron.removeAllListeners(
      EVENTS.DB_SELECT_TASK_SUCCESS, EVENTS.DB_DELETE_TASK_SUCCESS
    );
  }

  color(name): number | string {
    let hash = 0;
    if (name.length === 0) { return hash; }
    for (let i = 0; i < name.length; i++) {
// tslint:disable-next-line: no-bitwise
      hash = name.charCodeAt(i) + ((hash << 5) - hash);
// tslint:disable-next-line: no-bitwise
      hash = hash & hash;
    }
    let color = '#';
    for (let i = 0; i < 3; i++) {
// tslint:disable-next-line: no-bitwise
      const value = (hash >> (i * 8)) & 255;
      color += ('00' + value.toString(16)).substr(-2);
    }
    return color;
  }

  onScroll(): void {
    this.pageIndex += 1;
    this.page.offset = this.PAGE_SIZE * this.pageIndex;
    this.loader.load(new Loader(true, '加载任务列表...'));
    this.electron.ipcRenderer.send(EVENTS.DB_SELECT_TASK, this.page);
  }

  onEdit($event: MouseEvent, task?: Task): void {
    this.router.navigate(task ? ['task-edit', {id: task.id}] : ['task-edit']);
  }

  onSchedule($event: MouseEvent, task: Task) {
    this.router.navigate(['task-schedule', {id: task.id}]);
  }

  onDelete($event: MouseEvent, task: Task) {
    this.loader.load(new Loader(true, `删除中...`));
    this.electron.ipcRenderer.send(EVENTS.DB_DELETE_TASK, task.id);
  }

}
