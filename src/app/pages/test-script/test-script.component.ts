import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { interval, Observable, Subscription } from 'rxjs';
import { ElectronService } from '../../providers/electron.service';
import { EVENTS } from '../../../../shared/events';

@Component({
  selector: 'app-test-script',
  templateUrl: './test-script.component.html',
  styleUrls: ['./test-script.component.scss']
})
export class TestScriptComponent implements OnInit, OnDestroy {

  @ViewChild('script', { static: true }) script;
  @ViewChild('returnEditor', { static: true }) returnEditor;

  PERIOD = 9;
  timer: Observable<number> = interval(this.PERIOD);
  timerSubscribe: Subscription;
  languages = ['json', 'html', 'text', 'javascript', 'markdown'];
  selected = 0;
  isRunning = false;
  isStopping = false;
  res: any = {};
  time = 0;
  runningSelected = 0;

  constructor(private electron: ElectronService) {
  }

  code = `
  const puppeteer = require('puppeteer');

  (async () => {
    const browser = await puppeteer.launch({
      ignoreHTTPSErrors: true,
      headless: false
    });
    const page = await browser.newPage();
    await page.setUserAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36");
    await page.goto('https://www.qegoo.cn/', { waitUntil: 'domcontentloaded', timeout: 10000 });
    const html = await page.content();
    await browser.close();

    return html;
  })();

  `;

  ngOnInit() {
    this.electron.ipcRenderer.on(EVENTS.PUPPETEER_TEST_RUN_SUCCESS, (event, args) => {
      if (typeof args.data !== 'string') { args.data = JSON.stringify(args.data); }

      this.timerSubscribe.unsubscribe();
      this.res.data = args.data;
      this.res.success = args.success;
      this.isRunning = false;
      this.returnEditor.editor.setValue(this.res.data);
    });

    this.electron.ipcRenderer.on(EVENTS.PUPPETEER_TEST_STOP_SCUCCESS, (event, args) => {
      this.timerSubscribe.unsubscribe();
      this.isRunning = false;
      this.isStopping = false;
    });

  }

  ngOnDestroy() {
    this.electron.removeAllListeners(
      EVENTS.PUPPETEER_TEST_RUN_SUCCESS,
      EVENTS.PUPPETEER_TEST_STOP_SCUCCESS
    );
  }

  onTest() {
    if (this.isRunning) { return; }
    this.isRunning = true;
    this.time = 0;
    this.res = {};
    this.returnEditor.editor.setValue('');
    this.electron.ipcRenderer.send(EVENTS.PUPPETEER_TEST_RUN, this.script.code);
    this.timerSubscribe = this.timer.subscribe(() => this.time += this.PERIOD);

    setTimeout(() => {
      this.selected = 1;
    }, 500);
  }

  onFormatChange(value) {
    this.returnEditor.setLanguage(value);
  }

  onStop() {
    this.isStopping = true;
    this.electron.ipcRenderer.send(EVENTS.PUPPETEER_TEST_STOP, this.script.code);
  }

}
