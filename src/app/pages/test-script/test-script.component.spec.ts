import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestScriptComponent } from './test-script.component';

describe('TaskFlowScriptComponent', () => {
  let component: TestScriptComponent;
  let fixture: ComponentFixture<TestScriptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestScriptComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestScriptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
