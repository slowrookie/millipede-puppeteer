import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { ElectronService } from '../../providers/electron.service';
import { Page } from '../../../../shared/page';
import { EVENTS } from '../../../../shared/events';
import { LoaderService, Loader } from '../../providers/loader.service';

@Component({
  selector: 'app-run',
  templateUrl: './run.component.html',
  styleUrls: ['./run.component.scss']
})
export class RunComponent implements OnInit, OnDestroy {
  constructor(
    private electron: ElectronService,
    private loader: LoaderService
  ) {

  }

  @Input() scheduleId: number;
  loading = true;
  pageIndex = 1;
  pageSize = 10;
  total = 0;
  page: Page = new Page(this.pageSize, 0, {}, [{active: 'created_at', direction: 'desc'}]);

  data = [];

  ngOnInit() {
    this.page.params.scheduleId = this.scheduleId;

    this.electron.ipcRenderer.send(EVENTS.DB_SELECT_RUN, this.page);

    this.electron.on(EVENTS.DB_SELECT_RUN_SUCCESS + this.scheduleId).subscribe(d => {
      this.data = d ? d[0] : [];
      this.total = d ? d[1][0].count : 0;
      this.loading = false;
    });

    this.electron.on(EVENTS.DB_DELETE_RUN_SUCCESS + this.scheduleId).subscribe(d => {
      this.loader.load(new Loader(false));
      this.electron.ipcRenderer.send(EVENTS.DB_SELECT_RUN, this.page);
    });

    // 准备工作完成
    this.electron.on(EVENTS.PUPPETEER_MANUAL_RUN_STATUS + this.scheduleId).subscribe(() => {
      this.electron.ipcRenderer.send(EVENTS.DB_SELECT_RUN, this.page);
    });
  }

  ngOnDestroy() {
    this.electron.removeAllListeners(
      EVENTS.DB_SELECT_RUN_SUCCESS + this.scheduleId,
      EVENTS.PUPPETEER_MANUAL_RUN_STATUS + this.scheduleId,
      EVENTS.DB_SELECT_RUN_SUCCESS,
      EVENTS.DB_DELETE_RUN_SUCCESS + this.scheduleId
    );
  }

  onSearchData() {
    this.loading = true;
    this.page.offset = this.pageSize * (this.pageIndex - 1);
    this.electron.ipcRenderer.send(EVENTS.DB_SELECT_RUN, this.page);
  }

  onDelete(runId: number) {
    this.loader.load(new Loader(true, '数据删除中...'));
    this.electron.ipcRenderer.send(EVENTS.DB_DELETE_RUN, {runId: runId, scheduleId: this.scheduleId});
  }

}
