import { Component, OnInit, OnDestroy } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { ElectronService } from '../../providers/electron.service';
import { EVENTS } from '../../../../shared/events';
import { Step } from '../../../../main-process/database/step-service';
import { LoaderService, Loader } from '../../providers/loader.service';

@Component({
  selector: 'app-data-view',
  templateUrl: './data-view.component.html',
  styleUrls: ['./data-view.component.scss']
})
export class DataViewComponent implements OnInit, OnDestroy {

  constructor(
    private location: Location,
    private route: ActivatedRoute,
    private electron: ElectronService,
    private loader: LoaderService
  ) { }

  steps: Array<Step> = [];

  ngOnInit() {
    this.route.paramMap.subscribe(paramMap => {
      this.loader.load(new Loader(true, '加载中...'));
      this.electron.ipcRenderer.send(EVENTS.DB_SELECT_STEP, paramMap.get('runId'));
    });

    this.electron.on(EVENTS.DB_SELECT_STEP_SUCCESS).subscribe(steps => {
      this.steps = steps;
      this.loader.load(new Loader(false));
    });

  }

  ngOnDestroy() {
    this.electron.removeAllListeners(
      EVENTS.DB_SELECT_STEP_SUCCESS
    );
  }

  onBack($event: MouseEvent) {
    this.location.back();
  }

  onSelectChange() {
    this.electron.removeAllListeners(
      EVENTS.DB_SELECT_DATA_SUCCESS
    );
  }

}
