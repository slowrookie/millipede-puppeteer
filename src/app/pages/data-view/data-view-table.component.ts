import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { ElectronService } from '../../providers/electron.service';
import { Page } from '../../../../shared/page';
import { EVENTS } from '../../../../shared/events';
import { Step } from '../../../../main-process/database/step-service';

@Component({
// tslint:disable-next-line: component-selector
  selector: 'data-view-table',
  template: `
  <nz-table #smallTable nzSize="small" nzFrontPagination="false" [nzLoading]="loading"
    [(nzPageIndex)]="pageIndex" [(nzPageSize)]="pageSize"
    [nzTotal]="total" (nzPageIndexChange)="onSearchData()" [nzData]="data">
      <thead>
        <tr>
          <th nzAlign="center">NO.</th>
          <th nzAlign="center">开始时间</th>
          <th nzAlign="center">结束时间</th>
          <th nzAlign="center">耗时(ms)</th>
          <th nzAlign="center">状态</th>
          <th nzAlign="center">结果</th>
        </tr>
      </thead>
      <tbody>
        <tr *ngFor="let data of smallTable.data; let i = index">
          <td nzAlign="center">{{ (pageIndex - 1) * 10 + (i + 1) }}</td>
          <td nzAlign="center">{{ data.created_at | date:"yyyy-MM-dd HH:mm:ss" }}</td>
          <td nzAlign="center">{{ data.completed_at | date:"yyyy-MM-dd HH:mm:ss" }}</td>
          <td nzAlign="center">{{ data.spend }}</td>
          <td nzAlign="center" nz-tooltip nzTitle="失败" *ngIf="data.status === 0">
            <i nz-icon nzType="close-circle" nzTheme="outline" style="color: #f5222d;"></i>
          </td>
          <td nzAlign="center" nz-tooltip nzTitle="完成" *ngIf="data.status === 1">
            <i nz-icon nzType="check-circle" nzTheme="outline" style="color: #52c41a;"></i>
          </td>
          <td nzAlign="center">
            <i nz-icon nzType="read" nzTheme="outline" (click)="onOk(data.result)"></i>
          </td>
        </tr>
      </tbody>
    </nz-table>

    <nz-modal [(nzVisible)]="isVisible" nzTitle="结果" [nzOkText]="null" nzWidth="60%" (nzOnCancel)="onCancel()">
      <div style="height: 500px; overflow: auto;">
        <textarea nz-input [(ngModel)]="result" rows="23"></textarea>
      </div>
    </nz-modal>

  `,
  styles: [
  ]
})
export class DataViewTableComponent implements OnInit, OnDestroy {

  constructor(
    private electron: ElectronService,
  ) {

  }

  @Input() step: Step;
  loading = true;
  pageIndex = 1;
  pageSize = 10;
  total = 0;
  page: Page = new Page(this.pageSize, 0, {}, [{active: 'created_at', direction: 'desc'}]);

  data = [];
  tableName: string;
  isVisible = false;
  result: string;

  ngOnInit() {
    this.tableName = `data_${this.step.task_id}_${this.step.schedule_id}_${this.step.run_id}_${this.step.id}`;

    this.electron.ipcRenderer.send(EVENTS.DB_SELECT_DATA, {tableName: this.tableName, page: this.page});

    this.electron.on(EVENTS.DB_SELECT_DATA_SUCCESS).subscribe(d => {
      this.data = d ? d[0] : [];
      this.total = d ? d[1][0].count : 0;
      this.data.forEach(data => {
        data.spend = Date.parse(data.completed_at) - Date.parse(data.created_at);
      });
      this.loading = false;
    });
  }

  ngOnDestroy() {
    this.electron.removeAllListeners(
    );
  }

  onSearchData() {
    this.loading = true;
    this.page.offset = this.pageSize * (this.pageIndex - 1);
    this.electron.ipcRenderer.send(EVENTS.DB_SELECT_DATA, {tableName: this.tableName, page: this.page});
  }

  onCancel() {
    this.isVisible = false;
  }

  onOk(result: string) {
    this.isVisible = true;
    this.result = result;
  }


}
