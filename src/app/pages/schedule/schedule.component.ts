import { Component, OnInit, KeyValueDiffer, KeyValueDiffers, DoCheck, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Schedule, Category } from '../../../../main-process/database/schedule-service';
import { LoaderService, Loader } from '../../providers/loader.service';
import { ElectronService } from '../../providers/electron.service';
import { EVENTS } from '../../../../shared/events';
import { range } from 'rxjs';
import { NzModalService } from 'ng-zorro-antd';
import * as cronParser from 'cron-parser';

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.scss']
})
export class ScheduleComponent implements OnInit, DoCheck, OnDestroy {

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private loader: LoaderService,
    private electron: ElectronService,
    private differs: KeyValueDiffers,
    private modal: NzModalService,
  ) {
    this.schedule.category = Category.MANUAL;
    this.schedule.enabled = false;
    this.scheduleDiffer = differs.find(this.schedule).create();
  }

  schedules: Array<Schedule> = [];
  isVisible = false;
  schedule: Schedule = new Schedule();
  validateFormStatus = false;
  scheduleDiffer: KeyValueDiffer<String, any>;
  validateCronExpression = true;
  validateCronList: Array<Date> = [];
  isPreparing = false;

  ngOnInit() {
    this.loader.load(new Loader(true, '加载执行列表...'));

    this.route.paramMap.subscribe(paramMap => {
      if (paramMap.keys.length) {
// tslint:disable-next-line: radix
        this.schedule.task_id = Number.parseInt(paramMap.get('id'));
        this.electron.ipcRenderer.send(EVENTS.DB_SELECT_SCHEDULE, { task_id: this.schedule.task_id });
      }
    });

    this.electron.on(EVENTS.DB_SELECT_SCHEDULE_SUCCESS).subscribe(d => {
      this.schedules = d;
      this.loader.load(new Loader(false));
    });

    this.electron.on(EVENTS.DB_INSERT_SCHEDULE_SUCCESS).subscribe(() => {
      this.loader.load(new Loader(false));
      this.loader.load(new Loader(true, '加载执行列表...'));
      this.electron.ipcRenderer.send(EVENTS.DB_SELECT_SCHEDULE, { task_id: this.schedule.task_id });
    });

    this.electron.on(EVENTS.DB_UPDATE_SCHEDULE_SUCCESS).subscribe(() => {
      this.loader.load(new Loader(false));
    });

    this.electron.on(EVENTS.DB_DELETE_SCHEDULE_SUCCESS).subscribe(() => {
      this.loader.load(new Loader(false));
    });

    this.electron.on(EVENTS.PUPPETEER_MANUAL_RUN_PREPARED).subscribe(() => {
      this.isPreparing = false;
    });

  }

  ngDoCheck() {
    const scheduleChange = this.scheduleDiffer.diff(this.schedule);
    if (scheduleChange) {
      this.validateFormStatus = this.validatorForm();
    }
  }

  ngOnDestroy() {
    this.electron.removeAllListeners(
      EVENTS.DB_SELECT_SCHEDULE_SUCCESS,
      EVENTS.DB_INSERT_SCHEDULE_SUCCESS,
      EVENTS.DB_UPDATE_SCHEDULE_SUCCESS,
      EVENTS.DB_DELETE_SCHEDULE_SUCCESS
    );
  }

  onSave(): void {
    if (!this.validatorForm()) { return; }
    this.loader.load(new Loader(true, '保存中...'));
    this.isVisible = false;
    this.electron.ipcRenderer.send(this.schedule.id ? EVENTS.DB_UPDATE_SCHEDULE : EVENTS.DB_INSERT_SCHEDULE, this.schedule);
  }

  onCancel(): void {
    this.isVisible = false;
  }

  onAdd($event: MouseEvent) {
    this.schedule = new Schedule(
      null,
      this.schedule.task_id,
      '',
      Category.MANUAL,
      null,
      null,
      null,
      null,
      false,
      new Date()
    );
    this.isVisible = true;
  }

  onEdit($event: MouseEvent, schedule: Schedule) {
    $event.stopPropagation();
    this.schedule = schedule;
    this.isVisible = true;
  }

  onDelete($event: MouseEvent, schedule: Schedule, index: number) {
    $event.stopPropagation();
    this.modal.confirm({
      nzTitle: `确认删除 "${schedule.name}"？`,
      nzOnOk: () => {
        this.loader.load(new Loader(true, '删除中...'));
        this.schedules.splice(index, 1);
        this.electron.ipcRenderer.send(EVENTS.DB_DELETE_SCHEDULE, schedule.id);
      }
    });
  }

  onRun($event: MouseEvent, schedule: Schedule) {
    $event.stopPropagation();
    this.electron.ipcRenderer.send(EVENTS.PUPPETEER_MANUAL_RUN, schedule.id);
    this.isPreparing = true;
  }

  onEnableSwitch($event: MouseEvent) {
    $event.stopPropagation();
  }

  onEnableChange(schedule: Schedule) {
    schedule.enabled = !schedule.enabled;
    this.loader.load(new Loader(true, '保存中...'));
    this.electron.ipcRenderer.send(EVENTS.DB_UPDATE_SCHEDULE, schedule);
  }

  onBack($event: MouseEvent) {
    this.location.back();
  }

  onCategoryChange(category: Category) {
    this.schedule = new Schedule(
      this.schedule.id,
      this.schedule.task_id,
      '',
      category,
      null,
      null,
      null,
      null,
      false,
      this.schedule.created_at
    );

    this.validateFormStatus = this.validatorForm();
  }

  validatorForm(): boolean {
    if (this.schedule.category === Category.MANUAL) {
      return !!this.schedule.name;
    } else if (this.schedule.category === Category.DATETIME) {
      return !!this.schedule.name && !!this.schedule.datetime;
    } else if (this.schedule.category === Category.PERIOD) {
      return !!this.schedule.name && !!this.schedule.period && !!this.schedule.time;
    } else if (this.schedule.category === Category.CRON) {
      if (!this.schedule.cron) {
        this.validateCronExpression = true;
        return !!this.schedule.name && !!this.schedule.cron;
      }
      try {
        const interval = cronParser.parseExpression(this.schedule.cron);
        this.validateCronExpression = true;
        this.validateCronList = [];
        range(1, 5).subscribe(i => this.validateCronList.push(interval.next().toDate()));
      } catch (err) {
        this.validateCronList = [];
        this.validateCronExpression = false;
      }
      return this.validateCronExpression;
    }
  }


}
