import { Component, OnDestroy } from '@angular/core';
import { ElectronService } from './providers/electron.service';
import { TranslateService } from '@ngx-translate/core';
import { AppConfig } from '../environments/environment';
import { EVENTS } from '../../shared/events';
import { NzNotificationService } from 'ng-zorro-antd';
import { LoaderService, Loader } from './providers/loader.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnDestroy {
  constructor(
    public electronService: ElectronService,
    private translate: TranslateService,
    private notification: NzNotificationService,
    private laoder: LoaderService
  ) {

    translate.setDefaultLang('en');
    console.log('AppConfig', AppConfig);

    if (electronService.isElectron()) {
      console.log('Mode electron');
      console.log('Electron ipcRenderer', electronService.ipcRenderer);
      console.log('NodeJS childProcess', electronService.childProcess);
    } else {
      console.log('Mode web');
    }

    this.electronService.ipcRenderer.on(EVENTS.DB_ERROR, (event, args) => {
      this.notification.error('数据库异常', args.toString());
      this.laoder.load(new Loader(false));
    });

  }

  ngOnDestroy() {
    this.electronService.removeAllListeners(EVENTS.DB_ERROR);
  }
}
