import { Injectable } from '@angular/core';

// If you import a module but never use any of the imported values other than as TypeScript types,
// the resulting javascript file will look as if you never imported the module at all.
import { ipcRenderer, webFrame, remote, shell } from 'electron';
import * as childProcess from 'child_process';
import * as fs from 'fs';
import { Observable, Observer } from 'rxjs';

@Injectable()
export class ElectronService {

  ipcRenderer: typeof ipcRenderer;
  webFrame: typeof webFrame;
  remote: typeof remote;
  childProcess: typeof childProcess;
  fs: typeof fs;
  shell: typeof shell;

  constructor() {
    // Conditional imports
    if (this.isElectron()) {
      this.ipcRenderer = window.require('electron').ipcRenderer;
      this.webFrame = window.require('electron').webFrame;
      this.remote = window.require('electron').remote;
      this.shell = window.require('electron').shell;

      this.childProcess = window.require('child_process');
      this.fs = window.require('fs');
    }
  }

  isElectron = () => {
    return window && window.process && window.process.type;
  }

  on(event: string): Observable<any> {
    return new Observable((obs: Observer<any>) => {
      ipcRenderer.on(event, (_event, args) => {
        obs.next(args);
      });
    });
  }

  once(event: string): Observable<any> {
    return new Observable((obs: Observer<any>) => {
      ipcRenderer.once(event, (_event, args) => {
        obs.next(args);
      });
    });
  }

  removeAllListeners(...channels) {
    channels.forEach(channel => ipcRenderer.removeAllListeners(channel));
  }


}
