import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {

  private loaderSubject = new Subject<any>();

  constructor() { }

  observable = this.loaderSubject.asObservable();

  load(loader: Loader) {
    this.loaderSubject.next(loader);
  }

}

export class Loader {
  loading: boolean;
  title: string;

  constructor(
    loading: boolean,
    title?: string
  ) {
    this.loading = loading;
    this.title = title;
  }
}
