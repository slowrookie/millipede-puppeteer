import { Component, OnInit } from '@angular/core';
import * as dns from 'dns';
import { interval, from } from 'rxjs';
import { map } from 'rxjs/operators';
import { NzNotificationService } from 'ng-zorro-antd';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  constructor(private notification: NzNotificationService) { }

  networkStatus = 'available';

  ngOnInit() {
    interval(3000).subscribe(() => {
      dns.resolve('www.github.com', (err) => {
        if (err) {
          console.log('Network unavailable!');
          this.networkStatus = 'unavailable';
          this.notification.warning('网络异常', err.toString());
        } else {
          console.log('Network available!');
          this.networkStatus = 'available';
        }
      });
    });

  }

}
