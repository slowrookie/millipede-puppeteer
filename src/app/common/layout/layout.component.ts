import { Component, OnInit, HostBinding, OnDestroy, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { concat } from 'rxjs';

import { ElectronService } from '../../providers/electron.service';
import { EVENTS } from '../../../../shared/events';
import { LoaderService, Loader } from '../../providers/loader.service';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit, OnDestroy {

  constructor(
    private electron: ElectronService,
    private router: Router,
    private loader: LoaderService
  ) { }

  ngOnInit() {
    this.loader.load(new Loader(true, '数据库初始化...'));
    this.electron.ipcRenderer.send(EVENTS.DB_INITIALIZE);
    this.electron.ipcRenderer.send(EVENTS.DB_CHECK_TABLES);

    concat(this.electron.once(EVENTS.DB_INITIALIZE_SUCCESS), this.electron.once(EVENTS.DB_CHECK_TABLES_SUCCESS))
      .subscribe(args => {
        this.loader.load(new Loader(false));
      });
  }

  ngOnDestroy() {
    this.electron.removeAllListeners(EVENTS.DB_INITIALIZE_SUCCESS, EVENTS.DB_CHECK_TABLES_SUCCESS);
  }

  onSchdule() {
    this.router.navigate(['/home']);
  }

  onTask() {
    this.router.navigate(['/task']);
  }

  onTestScript() {
    this.router.navigate(['/test-script']);
  }

  onSettings() {
    this.router.navigate(['/settings']);
  }

}


