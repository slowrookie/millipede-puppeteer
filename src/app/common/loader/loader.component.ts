import { Component, OnInit, Input } from '@angular/core';
import { LoaderService, Loader } from '../../providers/loader.service';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})
export class LoaderComponent implements OnInit {

  constructor(
    private loader: LoaderService
  ) { }

  @Input() isLoading = false;
  @Input() title = 'Loading...';

  ngOnInit() {
    this.loader.observable.subscribe((b: Loader) => {
      this.isLoading = b.loading;
      this.title = b.title;
    });
  }

}
