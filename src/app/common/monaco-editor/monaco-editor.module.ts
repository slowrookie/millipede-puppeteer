import { NgModule } from '@angular/core';
import { MonacoEditorComponent } from './components/monaco-editor/monaco-editor.component';

@NgModule({
  imports: [],
  declarations: [
    MonacoEditorComponent,
  ],
  exports: [
    MonacoEditorComponent,
  ],
  entryComponents: [MonacoEditorComponent]
})
export class MonacoEditorModule { }
