import {
  Component,
  ViewChild,
  ElementRef,
  OnInit,
  OnChanges,
  OnDestroy,
  Input,
  Output,
  EventEmitter,
  ChangeDetectionStrategy
} from '@angular/core';
import { MonacoEditorLoaderService } from '../../services/monaco-editor-loader.service';
import { MonacoOptions } from '../../interfaces/monaco-options';

declare const monaco: any;

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'ngx-monaco-editor',
  template: `
    <div #container class="editor-container">
      <div #editor class="wrapper monaco-editor"></div>
    </div>
  `,
  styles: [
    `:host {
        flex: 1;
        box-sizing: border-box;
        flex-direction: column;
        display: flex;
        overflow: hidden;
        max-width: 100%;
        min-wdith: 0;
      }
      .wrapper {
        width: 100%; height: 100%;
      }
      .editor-container {
        text-overflow: ellipsis;
        overflow: hidden;
        position: relative;
        min-width: 0;
        display: table;
        width: 100%;
        height: 100%;
    }`
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MonacoEditorComponent implements OnInit, OnChanges, OnDestroy {
  container: HTMLDivElement;
  editor: any;

  @Input() code: string;
  @Input() options: MonacoOptions;
  @Output() codeChange = new EventEmitter<string>();

  @ViewChild('editor', { static: true }) editorContent: ElementRef;

  constructor(private monacoLoader: MonacoEditorLoaderService) { }

  ngOnInit() {
    this.container = this.editorContent.nativeElement;
    this.codeChange.next(this.code);
    this.monacoLoader.isMonacoLoaded.subscribe(value => {
      if (value) {
        this.initMonaco();
      }
    });
  }

  ngOnChanges(changes) {
    if (this.editor && (changes.code || changes.original)) {
      // this.editor.setValue(changes.code.currentValue);

      if (!this.options.language) {
        this.options.language = 'text';
      }
      monaco.editor.setModelLanguage(
        this.editor.getModel(),
        this.options.language
      );
    }
    if (this.editor && changes.options && changes.options.currentValue && changes.options.currentValue.theme) {
      monaco.editor.setTheme(changes.options.currentValue.theme);
    }
  }

  setLanguage(language: string) {
    monaco.editor.setModelLanguage(this.editor.getModel(), language);
  }

  private initMonaco() {

    const opts: any = {
      value: [this.code].join('\n'),
      language: 'json',
      automaticLayout: true,
      scrollBeyondLastLine: false,
      fontSize: 12
    };
    if (this.options.minimap) {
      opts.minimap = this.options.minimap;
    }
    if (this.options.readOnly) {
      opts['readOnly'] = true;
    }
    this.editor = monaco.editor.create(this.container, opts);
    this.editor.layout();

    if (this.options.automaticFormat) {
      this.editor.getAction('editor.action.formatDocument').run();
    }

    this.editor.updateOptions({
      'autoIndent': true,
      'formatOnPaste': true,
      'formatOnType': true
    });

    this.editor.onDidChangeModelContent(editor => {
      this.codeChange.next(this.editor.getValue());
    });
    if (this.options.theme) {
      monaco.editor.setTheme(this.options.theme);
    }

    if (!this.options.language) {
      this.options.language = 'text';
    }
    monaco.editor.setModelLanguage(
      this.editor.getModel(),
      this.options.language
    );
  }

  ngOnDestroy() {
    if (this.editor) {
      this.editor.dispose();
    }
  }
}
