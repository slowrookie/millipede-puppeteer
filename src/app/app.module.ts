import 'reflect-metadata';
import '../polyfills';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { HttpClientModule, HttpClient } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';

// NG Translate
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { ElectronService } from './providers/electron.service';

import { WebviewDirective } from './directives/webview.directive';

import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// antd
import { NgZorroAntdModule, NZ_I18N, zh_CN } from 'ng-zorro-antd';
import { registerLocaleData } from '@angular/common';
import zh from '@angular/common/locales/zh';
registerLocaleData(zh);

// ngx-infinite-scroll
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

// monaco editor
import { MonacoEditorModule } from './common/monaco-editor/monaco-editor.module';

// componets
import { LayoutComponent } from './common/layout/layout.component';
import { TaskComponent } from './pages/task/task.component';
import { TestScriptComponent } from './pages/test-script/test-script.component';
import { SettingsComponent } from './pages/settings/settings.component';
import { TaskEditComponent } from './pages/task/edit/task-edit.component';
import { FooterComponent } from './common/footer/footer.component';
import { LoaderComponent } from './common/loader/loader.component';
import { LoaderService } from './providers/loader.service';
import { ScheduleComponent } from './pages/schedule/schedule.component';
import { DataViewComponent } from './pages/data-view/data-view.component';
import { DataViewTableComponent } from './pages/data-view/data-view-table.component';
import { RunComponent } from './pages/run/run.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    WebviewDirective,
    LayoutComponent,
    TaskComponent,
    TestScriptComponent,
    SettingsComponent,
    TaskEditComponent,
    FooterComponent,
    LoaderComponent,
    ScheduleComponent,
    DataViewComponent,
    DataViewTableComponent,
    RunComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (HttpLoaderFactory),
        deps: [HttpClient]
      }
    }),
    NgZorroAntdModule,
    InfiniteScrollModule,
    MonacoEditorModule
  ],
  providers: [
    ElectronService,
    LoaderService,
    {provide: NZ_I18N, useValue: zh_CN}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
