import { HomeComponent } from './pages/home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './common/layout/layout.component';
import { TaskComponent } from './pages/task/task.component';
import { TaskEditComponent } from './pages/task/edit/task-edit.component';
import { TestScriptComponent } from './pages/test-script/test-script.component';
import { SettingsComponent } from './pages/settings/settings.component';
import { ScheduleComponent } from './pages/schedule/schedule.component';
import { DataViewComponent } from './pages/data-view/data-view.component';

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: '', redirectTo: 'task', pathMatch: 'full' },
            { path: 'home', component: HomeComponent },
            { path: 'task', component: TaskComponent },
            { path: 'task-edit', component: TaskEditComponent },
            { path: 'task-schedule', component: ScheduleComponent },
            { path: 'test-script', component: TestScriptComponent },
            { path: 'settings', component: SettingsComponent },
            { path: 'data-view', component: DataViewComponent },
        ]
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes, { useHash: true })],
    exports: [RouterModule]
})
export class AppRoutingModule { }
