export class Page {
  sorts: Array<PageSort>;
  limit: number;
  offset: number;
  total: number;
  params;

  constructor(limit: number = 0, offset: number = 0, params: any = {}, sorts: Array<any> = []) {
    this.limit = limit;
    this.offset = offset;
    this.params = params;
    this.sorts = sorts;
  }

  _sorts: Array<PageSort> = [];

  sort(pageSort: PageSort) {
    this.sorts.forEach(s => {
      if (s.active = pageSort.active) { s.direction = pageSort.direction; }
      this._sorts.push(s);
    });
    this.sorts = this._sorts;
    this._sorts = [];
  }

}

export class PageSort {
  active: string;
  direction: string;

  constructor(active: string, direction: string) {
    this.active = active;
    this.direction = direction;
  }
}
